package com.pajato.argus.network.adapter

import com.pajato.argus.network.adapter.ArgusNetworkRepo.injectDependency
import com.pajato.argus.network.core.I18nStrings.registerStrings
import com.pajato.argus.network.core.Network
import com.pajato.argus.network.core.SelectableNetwork
import com.pajato.dependency.uri.validator.UriValidationError
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.io.File
import java.net.URI
import kotlin.io.path.toPath
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.fail

class InjectDependencyUnitTest : ReportingTestProfiler() {
    private val loader: ClassLoader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val name = "files/networks.txt"
        val uri: URI = loader.getResource(name)?.toURI() ?: fail(URI_ERROR)
        copyResourceDirs(loader, "read-only-files", "files")
        registerStrings()
        runBlocking { injectDependency(uri) }
    }

    @Test fun `When fetching networks from a non-file, verify exception`() {
        val uri = loader.getResource("NetworksDir-Empty")?.toURI() ?: fail("Invalid test URI!")
        val expected = "com.pajato.dependency.uri.validator.UriValidationError: " +
            "java.lang.IllegalArgumentException: The argument 'uri' is not a file URI!"
        runBlocking { assertFailsWith<UriValidationError> { injectDependency(uri) } }.also {
            assertEquals(expected, it.message)
        }
    }

    @Test fun `When fetching data from a persistence file that does not exist, verify the repo size is 1`() {
        val uri = loader.getResource("NetworksDir-Empty")?.toURI() ?: fail("Invalid test directory URI!")
        val file = File(uri.toPath().toFile(), "no-such-file")
        runBlocking { injectDependency(file.toURI()) }
        assertEquals(0, ArgusNetworkRepo.cache.size)
        file.delete()
    }

    @Test fun `When creating a new persistence file, verify the new repo size is 1`() {
        val uri = loader.getResource("NetworksDir-NoFiles/dummy.txt")?.toURI() ?: fail("Invalid test URI!")
        runBlocking { injectDependency(uri) }
        assertEquals(0, ArgusNetworkRepo.cache.size)
    }

    @Test fun `When injecting an empty file and registering a new network, verify the new repo size is 2`() {
        val resourceName = "NetworksDir-Empty/networks.txt"
        val uri = loader.getResource(resourceName)?.toURI() ?: fail("Invalid test directory URI!")
        runBlocking { runTest(uri, SelectableNetwork(false, false, Network(27))) }
    }

    private suspend fun runTest(uri: URI, network: SelectableNetwork) {
        fun assert() { assertEquals(1, ArgusNetworkRepo.cache.size) }
        injectDependency(uri)
        ArgusNetworkRepo.register(network)
        assert()
    }

    private companion object { const val URI_ERROR = "Could not get the normal networks base resource URI!" }
}
