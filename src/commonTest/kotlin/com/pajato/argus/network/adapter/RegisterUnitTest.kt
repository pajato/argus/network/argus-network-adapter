package com.pajato.argus.network.adapter

import com.pajato.argus.network.adapter.ArgusNetworkRepo.injectDependency
import com.pajato.argus.network.adapter.ArgusNetworkRepo.networkUri
import com.pajato.argus.network.adapter.ArgusNetworkRepo.register
import com.pajato.argus.network.core.I18nStrings
import com.pajato.argus.network.core.I18nStrings.NETWORK_URI_ERROR
import com.pajato.argus.network.core.Network
import com.pajato.argus.network.core.NetworkRepoError
import com.pajato.argus.network.core.SelectableNetwork
import com.pajato.i18n.strings.StringsResource.get
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.fail

class RegisterUnitTest : ReportingTestProfiler() {
    private val loader: ClassLoader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val name = "files/networks.txt"
        val uri: URI = loader.getResource(name)?.toURI() ?: fail(URI_ERROR)
        copyResourceDirs(loader, "read-only-files", "files")
        I18nStrings.registerStrings()
        runBlocking { injectDependency(uri) }
    }

    @Test fun `When registering a network without injecting a URI using JSON data, verify the exception`() {
        val expected = get(NETWORK_URI_ERROR)
        networkUri = null
        assertFailsWith<NetworkRepoError> { register("") }.also { assertEquals(expected, it.message) }
    }

    @Test fun `When registering a network without injecting a URI using item data, verify the exception`() {
        val item = SelectableNetwork(false, false, Network(0))
        val expected = get(NETWORK_URI_ERROR)
        networkUri = null
        assertFailsWith<NetworkRepoError> { register(item) }.also { assertEquals(expected, it.message) }
    }

    @Test fun `When registering after injecting a valid URI, verify correct behavior`() {
        val itemUrl = loader.getResource("item.json") ?: fail("Could not load the resource 'item.json'")
        val json = itemUrl.readText().trim()
        register(json)
        assertCache()
    }

    private fun assertCache() {
        val item = ArgusNetworkRepo.cache[0] ?: fail("Item with id == 0 failed to register!")
        assertEquals(12, ArgusNetworkRepo.cache.size)
        assertEquals(true, item.isSelected)
        assertEquals("Pajato LLC", item.network.label)
        assertEquals("Pajato+", item.network.shortLabel)
        assertEquals("https://pajato.com/8VCV78prwd9QzZnEm0ReO6bERDa.jpg", item.network.iconUrl)
        assertEquals("com.pajato.argus.android", item.network.packageId)
    }

    private companion object {
        const val URI_ERROR = "Could not get the normal networks resource URI!"
    }
}
