package com.pajato.argus.network.adapter

import com.pajato.argus.network.adapter.ArgusNetworkRepo.injectDependency
import com.pajato.argus.network.core.I18nStrings
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlin.test.fail

class CacheUnitTest : ReportingTestProfiler() {
    private val loader: ClassLoader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val name = "files/networks.txt"
        val uri: URI = loader.getResource(name)?.toURI() ?: fail(URI_ERROR)
        copyResourceDirs(loader, "read-only-files", "files")
        I18nStrings.registerStrings()
        runBlocking { injectDependency(uri) }
    }

    @Test fun `When fetching repo data, verify a few cache operations`() {
        assertEquals(11, ArgusNetworkRepo.cache.size)
        assertTrue(ArgusNetworkRepo.cache[209] != null)
        assertTrue(ArgusNetworkRepo.cache[0] == null)
        assertFalse(ArgusNetworkRepo.cache.containsKey(1234))
        assertFalse(ArgusNetworkRepo.cache.containsKey(-256))
    }

    companion object { const val URI_ERROR = "Could not get the normal networks resource URI!" }
}
