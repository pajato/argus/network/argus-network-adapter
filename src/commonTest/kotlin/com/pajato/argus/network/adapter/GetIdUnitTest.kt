package com.pajato.argus.network.adapter

import com.pajato.argus.network.core.Network
import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class GetIdUnitTest : ReportingTestProfiler() {
    @Test fun `When an id is obtained for a network via getId, verify the result`() {
        val network = Network(23)
        assertEquals(23, getId(network))
    }
}
