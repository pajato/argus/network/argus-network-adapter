package com.pajato.argus.network.adapter

import com.pajato.argus.network.adapter.ArgusNetworkRepo.injectDependency
import com.pajato.argus.network.core.I18nStrings
import com.pajato.argus.network.core.SelectableNetwork
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class ToggleUnitTest : ReportingTestProfiler() {
    private val loader: ClassLoader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val name = "files/networks.txt"
        val uri: URI = loader.getResource(name)?.toURI() ?: fail(URI_ERROR)
        copyResourceDirs(loader, "read-only-files", "files")
        I18nStrings.registerStrings()
        runBlocking { injectDependency(uri) }
    }

    @Test fun `When toggling the isSelected property, verify behavior`() {
        val expected = getItem().isSelected.not()
        ArgusNetworkRepo.toggleSelected(getItem())
        assertEquals(expected, getItem().isSelected)
    }

    @Test fun `When toggling the isHidden property, verify behavior`() {
        val expected = getItem().isHidden.not()
        ArgusNetworkRepo.toggleHidden(getItem())
        assertEquals(expected, getItem().isHidden)
    }

    private fun getItem(): SelectableNetwork = ArgusNetworkRepo.cache[2552] ?: fail(GET_ITEM_FAIL_MESSAGE)

    private companion object {
        const val URI_ERROR = "Could not get the normal networks base resource URI!"
        const val GET_ITEM_FAIL_MESSAGE = "The item with id == 2552 was not injected!"
    }
}
