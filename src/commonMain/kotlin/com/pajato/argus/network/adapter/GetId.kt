package com.pajato.argus.network.adapter

import com.pajato.argus.network.core.Network

public fun getId(item: Network): Int = item.id
