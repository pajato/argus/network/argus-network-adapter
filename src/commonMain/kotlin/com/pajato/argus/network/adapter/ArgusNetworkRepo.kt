package com.pajato.argus.network.adapter

import com.pajato.argus.network.core.I18nStrings.NETWORK_URI_ERROR
import com.pajato.argus.network.core.NetworkCache
import com.pajato.argus.network.core.NetworkRepo
import com.pajato.argus.network.core.NetworkRepoError
import com.pajato.argus.network.core.SelectableNetwork
import com.pajato.dependency.uri.validator.validateUri
import com.pajato.i18n.strings.StringsResource.get
import com.pajato.persister.jsonFormat
import com.pajato.persister.persist
import com.pajato.persister.readAndPruneData
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.net.URI

public object ArgusNetworkRepo : NetworkRepo {
    override val cache: NetworkCache = mutableMapOf()
    internal var networkUri: URI? = null

    override suspend fun injectDependency(uri: URI) {
        validateUri(uri, ::handler)
        readAndPruneData(uri, cache, SelectableNetwork.serializer(), ::getKeyFromItem)
        networkUri = uri
    }

    private fun handler(cause: Throwable) { throw cause }

    private fun getKeyFromItem(item: SelectableNetwork) = item.network.id

    override fun register(json: String) {
        val uri = networkUri ?: throw NetworkRepoError(get(NETWORK_URI_ERROR))
        val item = jsonFormat.decodeFromString(SelectableNetwork.serializer(), json)
        cache[item.network.id] = item
        runBlocking { launch(IO) { persist(uri, json) } }
    }

    override fun register(item: SelectableNetwork) {
        val uri = networkUri ?: throw NetworkRepoError(get(NETWORK_URI_ERROR))
        val json = jsonFormat.encodeToString(SelectableNetwork.serializer(), item)
        cache[item.network.id] = item
        runBlocking { launch(IO) { persist(uri, json) } }
    }

    override fun toggleHidden(item: SelectableNetwork): Unit = register(item.copy(isHidden = !item.isHidden))

    override fun toggleSelected(item: SelectableNetwork): Unit = register(item.copy(isSelected = !item.isSelected))
}
