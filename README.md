# argus-network-adapter

## Description

This project implements the
[Clean Code Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
"Interface Adapters" layer for the network feature. The layer is responsible for handling data conversions between
core interfaces and persistable objects in order to execute use cases. It does this using Kotlin serialization and a
file system persistence store mechanism.

## License

GPL, Version 3.0.  See the peer document LICENSE for details.

## Contributions

See the [contributing guide](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/CONTRIBUTING.md) in the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

## Project status

Converted to Kotlin Multiplatform (KMP) with versions 0.10.*

## Documentation

For general documentation on Argus, see the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

As documentation entered into code files grows stale seconds after it is written, no such documentation is created.
Instead, documentation is created by you on demand using the Dokka Gradle task: 'dokkaGfm'. After successful task
completion, see the detailed documentation [here](build/dokka/gfm/index.md)

## Usage

To use the project, follow these steps:

1. Add the project as a dependency in your build file.
2. Import the necessary classes and interfaces from the project.
3. Use the provided APIs to interact with the shelf feature.

## Test Cases

### Overview

The table below identifies the adapter layer unit tests. A test file name is always of the form `NamePrefixUnitTest.kt`.
The test file content is one or more test cases (functions)

| Filename Prefix  | Test Case Name                                                                            |
|------------------|-------------------------------------------------------------------------------------------|
| Cache            | When fetching repo data, verify a few cache operations                                    |
| GetId            | When an id is obtained for a network via getId, verify the result                         |
| InjectDependency | When fetching networks from a non-file, verify exception                                  |
|                  | When fetching data from a persistence file that does not exist, verify the repo size is 1 |
|                  | When creating a new persistence file, verify the new repo size is 1                       |
|                  | When injecting an empty file and registering a new network, verify the repo size is 2     |
| Register         | When registering a network without injecting a URI using JSON data, verify the exception  |
|                  | When registering a network without injecting a URI using item data, verify the exception  |
|                  | When registering after injecting a valid URI, verify correct behavior                     |
| Remove           | When removing a network without injecting a URI using item data, verify the exception     |
|                  | When removing a network after injecting a valid URI, verify correct behavior              |
| Toggle           | When toggling the isSelected property, verify behavior                                    |

### Notes

The single responsibility for this project is to provide an interface adapter layer that handles data conversions
between network entity interfaces and persistable objects in order to execute network use cases.

Examples of network adapter artifacts are SerializableNetwork and ArgusNetworkRepo.
